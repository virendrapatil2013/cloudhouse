from django.urls import path,include
from django.contrib import admin
from django.contrib.auth import views as auth_view
from django.contrib.auth import urls

from . import views
urlpatterns = [
    path('', views.loginpage),
    path('index/', views.home, name='index'),
    ##path('catalog/', views.catalog),
    path('about/', views.about, name='about'),
    path('services/', views.services, name ='services'),
    path('testimonials/', views.testimonials, name='testimonials'),
    path('contact/', views.contact, name='contact'),
    path('admin/', admin.site.urls),




]