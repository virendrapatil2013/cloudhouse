from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from .models import Book, Author


# Create your views here.

def loginpage(request):
    return render(request, 'login.html')

@login_required
def home(request):
    num_books = Book.objects.all().count()
    num_authors = Author.objects.all().count()

    context = {
        'num_books': num_books,
        'num_authors': num_authors
    }
    return render(request, 'index.html', context=context)


def catalog(request):
    return HttpResponse('This is catalog page')


def about(request):
    context = {

    }
    return render(request, 'about.html', context=context)


def services(request):
    return render(request, 'services.html')


def testimonials(request):
    return render(request, 'testimonials.html')


def contact(request):
    return render(request, 'contact.html')





